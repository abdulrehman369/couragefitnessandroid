//package com.dev_saar.fitnessapp.adapter
//
//import com.affron.mp.base.BaseRecyclerViewAdapter
//import com.dev_saar.fitnessapp.R
//import com.dev_saar.fitnessapp.databinding.IvPhotoGalleryBinding
//import com.dev_saar.fitnessapp.model.Gallery
//
//class GalleryAdapter : BaseRecyclerViewAdapter<Gallery,IvPhotoGalleryBinding>() {
//
//    override fun getLayout() = R.layout.iv_photo_gallery
//
//    override fun onBindViewHolder(
//        holder: Companion.BaseViewHolder<IvPhotoGalleryBinding>,
//        position: Int
//    ) {
//
//        holder.binding.apply {
//            gallery = items[position]
//
//            bodyImg.setImageResource(items[position].builderImg!!)
//            root.setOnClickListener {
//                listener?.invoke(it, items[position], position)
//            }
//
//            tvDateYear.setText(items[position].titleDate!!)
//            root.setOnClickListener {
//                listener?.invoke(it, items[position], position)
//            }
//            tvLibs.setText(items[position].numberLbs!!)
//            root.setOnClickListener {
//                listener?.invoke(it, items[position], position)
//            }
//
//        }
//
//    }
//}