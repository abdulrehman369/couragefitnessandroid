package com.dev_saar.fitnessapp.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev_saar.fitnessapp.R
import com.dev_saar.fitnessapp.model.addImagesModels.UserImageMainModel


class GetPhotoAdapter(
        private val context: Context,
        private val raceModel: List<UserImageMainModel>
) :
    RecyclerView.Adapter<GetPhotoAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.body_img)
        val date: TextView = itemView.findViewById(R.id.tv_date_year)
        val tv_libs: TextView = itemView.findViewById(R.id.tv_libs)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.iv_photo_gallery, parent, false)
        return ViewHolder(itemView)
    }
    lateinit var finalDates: String

    var isImageFitToScreen = false
    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel[position]
        holder.date.text = model.image_date
        holder.tv_libs.text = model.weight



        Glide.with(context)
            .load(model.image_url)
            .placeholder(R.color.white_gray).into(holder.image)
        holder.itemView.setOnClickListener {
            showDetailDialogForImageInfFullSize(model.image_url)
        }
    }

    override fun getItemCount() = raceModel.size
    private fun showDetailDialogForImageInfFullSize(imageUrl: String) {
        val metrics = DisplayMetrics()
        val windowManager = context
            .getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(metrics)
        val height = metrics.heightPixels
        val width = metrics.widthPixels
        val dialog: Dialog
        dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialogforimage)
        dialog.window!!.setLayout(
            (width * 0.9).toInt(),
            (height * 0.70).toInt()
        ) //Controlling width and height.
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val imgclose = dialog.findViewById<ImageView>(R.id.ic_cancel)
        val imageINFullSize = dialog.findViewById<ImageView>(R.id.image)

        Glide.with(context)
            .load(imageUrl)
            .placeholder(R.color.white_gray).into(imageINFullSize)
        imgclose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

}
