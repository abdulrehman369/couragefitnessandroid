package com.dev_saar.fitnessapp.adapter

import com.affron.mp.base.BaseRecyclerViewAdapter
import com.dev_saar.fitnessapp.R
import com.dev_saar.fitnessapp.databinding.IvNutritionPlanBinding
import com.dev_saar.fitnessapp.databinding.IvPhotoGalleryBinding
import com.dev_saar.fitnessapp.model.Gallery
import com.dev_saar.fitnessapp.model.Nutrition

//class NutritionAdapter : BaseRecyclerViewAdapter<Nutrition, IvNutritionPlanBinding>() {
//
//    override fun getLayout() = R.layout.iv_nutrition_plan
//
//    override fun onBindViewHolder(
//        holder: Companion.BaseViewHolder<IvNutritionPlanBinding>,
//        position: Int
//    ) {
//
//        holder.binding.apply {
//            gallery = items[position]
//
//            bodyImg.setImageResource(items[position].builderImg!!)
//            root.setOnClickListener {
//                listener?.invoke(it, items[position], position)
//            }
//
//    }
//}