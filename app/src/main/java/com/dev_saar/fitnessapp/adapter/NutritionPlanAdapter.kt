package com.dev_saar.fitnessapp.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.dev_saar.fitnessapp.R
import com.dev_saar.fitnessapp.externalClasses.PermissionUtils
import com.dev_saar.fitnessapp.model.dwnloadPDF.DownloadPdfDataModel
import java.io.File


class NutritionPlanAdapter(
        private val context: Context,
        private val raceModel: List<DownloadPdfDataModel>
) :
        RecyclerView.Adapter<NutritionPlanAdapter.ViewHolder>() {
    private val STORAGE_PERMISSION_REQUEST_CODE = 1

    var permissionUtils: PermissionUtils? = null

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tv_file_handling)
        val tvHeading: TextView = itemView.findViewById(R.id.tvHeading)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
                LayoutInflater.from(parent.context).inflate(R.layout.iv_nutrition_plan, parent, false)
        return ViewHolder(itemView)
    }

    lateinit var finalDates: String

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        permissionUtils = PermissionUtils()
        //
        val model = raceModel[position]
        holder.title.text = model.pdf_file_name
        holder.tvHeading.text = model.created_at


//
//        Glide.with(context)
//            .load(model.image_url)
//            .placeholder(R.color.white_gray).into(holder.image)
        holder.itemView.setOnClickListener {
            if (permissionUtils!!.checkPermission(context as Activity, STORAGE_PERMISSION_REQUEST_CODE, it)) {

                try {
                    context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(model.pdf_file_link)))
                } catch (e: Exception) {
                    e.stackTrace
                }

            }
        }
    }



    override fun getItemCount() = raceModel.size
}

