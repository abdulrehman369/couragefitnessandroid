package com.dev_saar.fitnessapp.externalClasses

import android.net.wifi.WifiConfiguration.AuthAlgorithm.strings
import android.os.AsyncTask
import android.os.Environment
import java.io.File
import java.io.IOException

class DownloadFile : AsyncTask<String?, Void?, Void?>() {
    override fun doInBackground(vararg params: String?): Void? {
        val fileUrl = strings[0] // -> http://maven.apache.org/maven-1.x/maven.pdf
        val fileName = strings[1] // -> maven.pdf
        val extStorageDirectory: String = Environment.getExternalStorageDirectory().toString()
        val folder = File(extStorageDirectory, "fitnessAppPDF_File")
        folder.mkdir()
        val pdfFile = File(folder, fileName)

        try {
            pdfFile.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        FileDownloader.downloadFile(fileUrl, pdfFile)
        return null
    }


}
