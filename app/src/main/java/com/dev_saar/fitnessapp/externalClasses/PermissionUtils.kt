package com.dev_saar.fitnessapp.externalClasses

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.dev_saar.fitnessapp.R
import com.google.android.material.snackbar.Snackbar

class PermissionUtils {
    fun checkPermission(activity: Activity, request_code: Int, view: View): Boolean {
        var result = 0
        when (request_code) {
            1 -> result = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            else -> {
            }
        }
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermission(activity, request_code, view)
            false
        }
    }

    fun requestPermission(activity: Activity, request_code: Int, view: View?) {
        when (request_code) {
            1 -> if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Snackbar.make(view!!, activity.getString(R.string.permission_request_message), Snackbar.LENGTH_LONG).setAction("Allow", View.OnClickListener { //Toast.makeText(activity, activity.getString(R.string.permission_request_message), Toast.LENGTH_LONG).show();
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + activity.packageName))
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    activity.startActivity(intent)
                }).show()
            } else {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), request_code)
            }
            else -> {
            }
        }
    }
}
