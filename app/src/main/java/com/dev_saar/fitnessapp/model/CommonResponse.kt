package com.dev_saar.fitnessapp.model

import com.google.gson.annotations.SerializedName

data class CommonResponse (
    @SerializedName("status")
    var statusCode: Int,

    @SerializedName("message")
    var message: String


)