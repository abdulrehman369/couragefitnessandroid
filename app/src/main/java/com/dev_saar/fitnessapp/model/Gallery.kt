package com.dev_saar.fitnessapp.model

import java.io.Serializable
import java.util.*

data class Gallery(
    var titleDate: String = "",
    var numberLbs : String = "",
    var builderImg : Int? = null,
) : Serializable
