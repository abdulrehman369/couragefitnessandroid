package com.dev_saar.fitnessapp.model

import java.io.Serializable

data class Nutrition(
    var titleFile: String = "",
    var titleDate : String = "",
) : Serializable
