package com.dev_saar.fitnessapp.model.addImagesModels

import com.google.gson.annotations.SerializedName

data class GetImageDataModel(

    @SerializedName("user_images")
    var suggestedList: ArrayList<UserImageMainModel>,

)