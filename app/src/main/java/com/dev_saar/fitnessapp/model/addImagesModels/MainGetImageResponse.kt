package com.dev_saar.fitnessapp.model.addImagesModels

import com.google.gson.annotations.SerializedName

data class MainGetImageResponse(
    @SerializedName("status")
    var statusCode: Int,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: GetImageDataModel
)