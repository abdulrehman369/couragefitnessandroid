package com.dev_saar.fitnessapp.model.addImagesModels

import com.google.gson.annotations.SerializedName

data class UserImageMainModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("image_name")
    var image_name: String,

    @SerializedName("image_url")
    var image_url: String,

    @SerializedName("weight")
    var weight: String,

    @SerializedName("image_date")
    var image_date: String,

)