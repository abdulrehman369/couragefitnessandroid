package com.dev_saar.fitnessapp.model.authModels

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("first_name")
    val first_name : String? = null,
    @SerializedName("last_name")
    val last_name : String? = null,
    @SerializedName("date_of_birth")
    val date_of_birth :String? = null,
    @SerializedName("email")
    val email: String? = null,

)

