package com.mtechsoft.compassapp.models.authModels

import com.google.gson.annotations.SerializedName

data class UserLatLngResponse(
    @SerializedName("message")
     val message: String?=null,
    @SerializedName("status")
    val status: Boolean?=null)