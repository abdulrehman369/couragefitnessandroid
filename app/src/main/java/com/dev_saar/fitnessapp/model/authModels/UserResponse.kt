package com.mtechsoft.compassapp.models.authModels

import com.dev_saar.fitnessapp.model.authModels.User
import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("status")
    var statusCode: Int,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: User
)