package com.dev_saar.fitnessapp.model.checkIn

import com.google.gson.annotations.SerializedName

data class CheckInDataModel(
    @SerializedName("previous_weight")
    val previous_weight: String? = null,
    @SerializedName("current_weight")
    val current_weight : String? = null,
    @SerializedName("comment")
    val comment : String? = null,
    @SerializedName("user_id")
    val user_id :String? = null,
    @SerializedName("created_at")
    val created_at: String? = null,
   @SerializedName("id")
    val id: Int? = null,

)

