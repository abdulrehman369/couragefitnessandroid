package com.mtechsoft.compassapp.models.authModels

import com.dev_saar.fitnessapp.model.authModels.User
import com.dev_saar.fitnessapp.model.checkIn.CheckInDataModel
import com.google.gson.annotations.SerializedName

data class CheckinResponse(
    @SerializedName("status")
    var statusCode: Int,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var data: CheckInDataModel
)