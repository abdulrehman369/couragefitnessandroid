package com.dev_saar.fitnessapp.model.dwnloadPDF

import com.dev_saar.fitnessapp.model.addImagesModels.UserImageMainModel
import com.google.gson.annotations.SerializedName

data class DownloadPdfDataModel(
        @SerializedName("id")
        var id: Int?,

        @SerializedName("pdf_file_name")
        var pdf_file_name: String,

        @SerializedName("pdf_file_link")
        var pdf_file_link: String,

        @SerializedName("created_at")
        var created_at: String,

        @SerializedName("updated_at")
        var updated_at: String,

        )