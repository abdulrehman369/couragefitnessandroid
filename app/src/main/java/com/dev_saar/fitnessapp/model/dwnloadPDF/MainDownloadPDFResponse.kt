package com.dev_saar.fitnessapp.model.dwnloadPDF

import com.dev_saar.fitnessapp.model.addImagesModels.GetImageDataModel
import com.dev_saar.fitnessapp.model.addImagesModels.UserImageMainModel
import com.google.gson.annotations.SerializedName

data class MainDownloadPDFResponse(
        @SerializedName("status")
    var statusCode: Int,

        @SerializedName("message")
    var message: String,
        @SerializedName("data")
    var suggestedList: ArrayList<DownloadPdfDataModel>,
)