package com.dev_saar.fitnessapp.services


object Constants {

    // Endpoints
    const val BASE_URL = "http://145.14.158.138/fitness-app/public/index.php/api/"
//    const val BASE_URL = "http://jsonplaceholder.typicode.com/"
    const val BASE_URL_IMG = "http://145.14.158.138/fitness-app/public/"

    const val LOGIN_URL = "login"
    const val POSTS_URL = "posts"

}