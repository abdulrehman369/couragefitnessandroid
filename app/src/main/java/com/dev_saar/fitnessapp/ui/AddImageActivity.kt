package com.dev_saar.fitnessapp.ui

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.widget.Toast
import com.dev_saar.fitnessapp.Utilities
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivityAddImageBinding
import com.dev_saar.fitnessapp.model.CommonResponse
import com.dev_saar.fitnessapp.services.ApiClient
import com.github.dhaval2404.imagepicker.ImagePicker
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File

class AddImageActivity : BaseActivityWithoutVM<ActivityAddImageBinding>() {
    private lateinit var apiClient: ApiClient
    var uri1: Uri? = null
    var image: File? = null
    var input: String = ""
    var userId: String = ""
    var selected_img_bitmap: Bitmap? = null
    override fun getViewBinding(): ActivityAddImageBinding =
            ActivityAddImageBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        apiClient = ApiClient()
        userId = Utilities.getString(this, Utilities.USER_ID)
        mViewBinding.apply {

            icBackArrow.setOnClickListener {

                onBackPressed()
            }
            ivImage.setOnClickListener {
//            FilePicker(this@CompanyInfoFragment).launch(1)
//            CropImage.activity().start(requireActivity())
                ImagePicker.with(this@AddImageActivity)
                        .crop()
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start()
            }
            checkInButton.setOnClickListener {

                val getWe8: String = etWeight.text.toString()
                if (!getWe8.equals("")) {
                    if (uri1 != null && !uri1!!.equals(Uri.EMPTY)) {
                        val byteArrayOutputStream = ByteArrayOutputStream()
                        selected_img_bitmap?.compress(
                                Bitmap.CompressFormat.JPEG,
                                70,
                                byteArrayOutputStream
                        )
                        val byteArray = byteArrayOutputStream.toByteArray()
                        val imageEncoded =
                                Base64.encodeToString(byteArray, Base64.NO_WRAP)
                        input = imageEncoded
                        input = input.replace("\n", "")
                        input = "data:image/png;base64," + input.trim { it <= ' ' }
                        callApi(getWe8, input, userId)
                    } else {
                        showToast("Please Upload a Image ")

                    }

                } else {
                    showToast("Enter  Weight")

                }
            }
        }
    }

    fun callApi(
            prevoiusW8: String,
            image: String,
            userId: String
    ) {
        val hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show()

        apiClient.getApiService().addImage(prevoiusW8, image, userId)
                .enqueue(object : Callback<CommonResponse> {
                    override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                        hud.dismiss()
                        Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                                .show()
                    }

                    override fun onResponse(
                            call: Call<CommonResponse>,
                            response: Response<CommonResponse>
                    ) {
                        val loginResponse = response.body()

                        if (loginResponse?.statusCode == 200) {
                            hud.dismiss()
                            showToast(loginResponse.message)
                            startActivity(Intent(this@AddImageActivity,PhotoGalleryActivity::class.java))
                            finish()

                        } else {
//                        val loginResponse = response.body()
                            hud.dismiss()
                            if (loginResponse != null) {
                                showToast(loginResponse.message)
                            }
                        }
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            uri1 = data?.data!!
            //            Glide.with(this).load(uri1).into(binding.imagePicker)
            selected_img_bitmap =
                    MediaStore.Images.Media.getBitmap(this.contentResolver, uri1)
            mViewBinding.apply {
                ivImage.setImageBitmap(selected_img_bitmap)
            }
//
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!
            //You can also get File Path from intent
            val filePath: String = ImagePicker.getFilePath(data)!!
            image = file
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
}