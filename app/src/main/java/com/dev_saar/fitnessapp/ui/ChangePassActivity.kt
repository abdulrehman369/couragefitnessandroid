package com.dev_saar.fitnessapp.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.dev_saar.fitnessapp.Utilities
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivityChangePassBinding
import com.dev_saar.fitnessapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import com.mtechsoft.compassapp.models.authModels.UserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePassActivity : BaseActivityWithoutVM<ActivityChangePassBinding>() {
    var userId: String = ""
    private lateinit var apiClient: ApiClient
    override fun getViewBinding(): ActivityChangePassBinding =
        ActivityChangePassBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        apiClient = ApiClient()
        userId = Utilities.getString(this, Utilities.USER_ID)
        mViewBinding.apply {

            icBackArrow.setOnClickListener {

                onBackPressed()
            }
            btnUpdatePass.setOnClickListener {

                val getCurrentPass: String = etCurrentPass.text.toString()
                val getNewPass: String = etNewPassword.text.toString()
                if (!getCurrentPass.equals("")) {
                    if (!getNewPass.equals("")) {
                                    callApi(userId,getCurrentPass,getNewPass)


                    } else {
                        showToast("Enter New Password to Continue")

                    }
                } else {
                    showToast("Enter Current Password to Continue")

                }
            }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_log_in)
        }
    }

    fun callApi(userId:String,currentPass: String, newPass: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().changePassApi(userId,currentPass, newPass)
            .enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == 200) {
                        hud.dismiss()
                        showToast(loginResponse.message)

                        val intent = Intent(this@ChangePassActivity, HomeActivity::class.java)
                        startActivity(intent)
                        finish()

                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }
}