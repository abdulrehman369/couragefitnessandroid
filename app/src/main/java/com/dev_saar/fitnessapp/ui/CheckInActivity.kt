package com.dev_saar.fitnessapp.ui

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Base64
import android.widget.Toast
import com.dev_saar.fitnessapp.Utilities
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivityCheckInBinding
import com.dev_saar.fitnessapp.model.CommonResponse
import com.dev_saar.fitnessapp.services.ApiClient
import com.github.dhaval2404.imagepicker.ImagePicker
import com.kaopiz.kprogresshud.KProgressHUD
import com.mtechsoft.compassapp.models.authModels.CheckinResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File

class CheckInActivity : BaseActivityWithoutVM<ActivityCheckInBinding>() {
    private lateinit var apiClient: ApiClient
    var uri1: Uri? = null
    var image: File? = null
    var input: String = ""
    var userId: String = ""
    var diff=0.0
    var selected_img_bitmap: Bitmap? = null
    override fun getViewBinding(): ActivityCheckInBinding =
            ActivityCheckInBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        apiClient = ApiClient()
        userId = Utilities.getString(this, Utilities.USER_ID)
        mViewBinding.apply {

            icBackArrow.setOnClickListener {

                onBackPressed()
            }
            ivImage.setOnClickListener {
//            FilePicker(this@CompanyInfoFragment).launch(1)
//            CropImage.activity().start(requireActivity())
                ImagePicker.with(this@CheckInActivity)
                        .crop()
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start()
            }
            getCurrentWe8.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable) {
                    if (TextUtils.isEmpty(getCurrentWe8.text.toString())) {
                        tvDifference.setText("0")
//                        showToast(getCurrentWe8.text.toString())
                        return
                    } else {
                        var diff=0.0
                        var firstValue:String=etPreviousWe8.text.toString()
                        var secondValue:String=getCurrentWe8.text.toString()

//                        val size: Float = Math.abs((getCurrentWe8.x - etPreviousWe8.x) * (getCurrentWe8.y - etPreviousWe8.y))
//                        tvDifference.text=size.toString()
                        if (firstValue.toDouble()>secondValue.toDouble()) {
                            diff = (firstValue.toDouble() - secondValue.toDouble());
                        } else if (secondValue.toDouble()>firstValue.toDouble()) {
                            diff = (secondValue.toDouble()-firstValue.toDouble())
                        } else if (firstValue.toDouble()==secondValue.toDouble()) {
                            diff = 0.0;
                        }
                        tvDifference.setText(diff.toString())
//                        showToast(getCurrentWe8.text.toString())
                    }
                }
            })
            etPreviousWe8.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable) {
                    if (TextUtils.isEmpty(getCurrentWe8.text.toString())) {
                        tvDifference.setText("0")
//                        showToast(getCurrentWe8.text.toString())
                        return
                    } else {

                        var firstValue:String=etPreviousWe8.text.toString()
                        var secondValue:String=getCurrentWe8.text.toString()

//                        val size: Float = Math.abs((getCurrentWe8.x - etPreviousWe8.x) * (getCurrentWe8.y - etPreviousWe8.y))
//                        tvDifference.text=size.toString()
                        if (firstValue.toDouble()>secondValue.toDouble()) {
                            diff = (firstValue.toDouble() - secondValue.toDouble());
                        } else if (secondValue.toDouble()>firstValue.toDouble()) {
                            diff = (secondValue.toDouble()-firstValue.toDouble())
                        } else if (firstValue.toDouble()==secondValue.toDouble()) {
                            diff = 0.0;
                        }
                        tvDifference.setText(diff.toString())
                    }
                }
            })
            checkInButton.setOnClickListener {

                val getPreviousWe8: String = etPreviousWe8.text.toString()
                val getCurrentWe88: String = getCurrentWe8.text.toString()
                val getComment: String = Comment.text.toString()
                if (!getPreviousWe8.equals("")) {
                    if (!getCurrentWe88.equals("")) {
                        if (!getComment.equals("")) {
                            if (uri1 != null && !uri1!!.equals(Uri.EMPTY)) {
                                val byteArrayOutputStream = ByteArrayOutputStream()
                                selected_img_bitmap?.compress(
                                        Bitmap.CompressFormat.JPEG,
                                        70,
                                        byteArrayOutputStream
                                )
                                val byteArray = byteArrayOutputStream.toByteArray()
                                val imageEncoded =
                                        Base64.encodeToString(byteArray, Base64.NO_WRAP)
                                input = imageEncoded
                                input = input.replace("\n", "")
                                input = "data:image/png;base64," + input.trim { it <= ' ' }
                                callApi(getPreviousWe8, getCurrentWe88, getComment, input, userId)
                            } else {
                                showToast("Please Upload a Image ")

                            }
                        } else {
                            showToast("Enter Comment")

                        }
                    } else {
                        showToast("Enter Current Weight")

                    }
                } else {
                    showToast("Enter Previous Weight")

                }
            }
        }
    }

    fun callApi(
            prevoiusW8: String,
            currentWe8: String,
            comment: String,
            image: String,
            userId: String
    ) {
        val hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show()

        apiClient.getApiService().checkIn(prevoiusW8, currentWe8, comment,image, userId)
                .enqueue(object : Callback<CheckinResponse> {
                    override fun onFailure(call: Call<CheckinResponse>, t: Throwable) {
                        hud.dismiss()
                        Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                                .show()
                    }

                    override fun onResponse(
                            call: Call<CheckinResponse>,
                            response: Response<CheckinResponse>
                    ) {
                        val loginResponse = response.body()

                        if (loginResponse?.statusCode == 200) {
                            hud.dismiss()
                            showToast(loginResponse.message)
//                        val differneceWeight=prevoiusW8-currentWe8
//                        addImageApi()
                            startActivity(Intent(this@CheckInActivity, HomeActivity::class.java))
                            finish()

                        } else {
//                        val loginResponse = response.body()
                            hud.dismiss()
                            if (loginResponse != null) {
                                showToast(loginResponse.message)
                            }
                        }
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            uri1 = data?.data!!
            //            Glide.with(this).load(uri1).into(binding.imagePicker)
            selected_img_bitmap =
                    MediaStore.Images.Media.getBitmap(this.contentResolver, uri1)
            mViewBinding.apply {
                ivImage.setImageBitmap(selected_img_bitmap)
            }
//
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!
            //You can also get File Path from intent
            val filePath: String = ImagePicker.getFilePath(data)!!
            image = file
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    fun addImageApi(
            prevoiusW8: String,
            image: String,
            userId: String
    ) {
        val hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show()

        apiClient.getApiService().addImage(prevoiusW8, image, userId)
                .enqueue(object : Callback<CommonResponse> {
                    override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                        hud.dismiss()
                        Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                                .show()
                    }

                    override fun onResponse(
                            call: Call<CommonResponse>,
                            response: Response<CommonResponse>
                    ) {
                        val loginResponse = response.body()

                        if (loginResponse?.statusCode == 200) {
                            hud.dismiss()
                            showToast(loginResponse.message)
                            finish()

                        } else {
//                        val loginResponse = response.body()
                            hud.dismiss()
                            if (loginResponse != null) {
                                showToast(loginResponse.message)
                            }
                        }
                    }
                })
    }
}