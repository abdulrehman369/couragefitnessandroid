package com.dev_saar.fitnessapp.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import com.dev_saar.fitnessapp.BuildConfig
import com.dev_saar.fitnessapp.R
import com.dev_saar.fitnessapp.Utilities
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivityHomeBinding
import com.google.android.material.navigation.NavigationView
import libs.mjn.prettydialog.PrettyDialog
import libs.mjn.prettydialog.PrettyDialogCallback

class HomeActivity : BaseActivityWithoutVM<ActivityHomeBinding>() {
    var navController: NavController? = null
    private var drawer: DrawerLayout? = null
    var logo: ImageView? = null
    var navigationView: NavigationView? = null
    override fun getViewBinding(): ActivityHomeBinding =
        ActivityHomeBinding.inflate(layoutInflater)

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("UseCompatLoadingForDrawables")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        navigationView=findViewById(R.id.nav_view)
        drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        mViewBinding.apply {

            ivMenu.setOnClickListener(View.OnClickListener {
                drawer!!.openDrawer(
                    GravityCompat.START,
                    true
                )
            })
//            navController = Navigation.findNavController(this@HomeActivity, R.id.user_container)
//            NavigationUI.setupWithNavController(navigationView!!, navController!!)
//            navController!!.addOnDestinationChangedListener { controller, destination, arguments ->
//                if (destination.label != null) {
//
//                }
//            }
            navigationView!!.menu.findItem(R.id.logout).setOnMenuItemClickListener {
                drawer!!.closeDrawer(GravityCompat.START, false)
                showCustomDialog()
                true
            }
    navigationView!!.menu.findItem(R.id.shareApp).setOnMenuItemClickListener {
                drawer!!.closeDrawer(GravityCompat.START, false)
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Courage Fit")
            var shareMessage = "\nLet me recommend you this application\n\n"
            shareMessage =
                """
                   ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                   
                   
                   """.trimIndent()
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            //e.toString();
        }
        true
            }

            navigationView!!.menu.findItem(R.id.home).setOnMenuItemClickListener {
                drawer!!.closeDrawer(GravityCompat.START, false)
                true
            }
            navigationView!!.menu.findItem(R.id.profile).setOnMenuItemClickListener {
                drawer!!.closeDrawer(GravityCompat.START, false)
     startActivity(Intent(this@HomeActivity, UpdateProfileActivity::class.java))
                true
            }
            mViewBinding.apply {


                    rlNutrition.setOnClickListener {
                        Utilities.saveString(this@HomeActivity, "type", "nutrition")

                        navigateToNextActivity(NutritionPlanActivity::class.java)
                    }
                    rlTrainig.setOnClickListener {
                        Utilities.saveString(this@HomeActivity, "type", "training")
                        navigateToNextActivity(NutritionPlanActivity::class.java)
                    }
                    rlGallery.setOnClickListener {
                        navigateToNextActivity(PhotoGalleryActivity::class.java)
                    }


                rlCheckIn.setOnClickListener {
                    navigateToNextActivity(CheckInActivity::class.java)

                }
            }
        }
    }

    private fun showCustomDialogforExit() {
        val pDialog = PrettyDialog(this)
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.color_primary)
                .addButton(
                    "Yes",
                    R.color.color_primary,
                    R.color.pdlg_color_white,
                    object : PrettyDialogCallback {
                        override fun onClick() {
                            pDialog.dismiss()
                          finish()
                            finishAffinity()
                        }
                    }
                )
                .addButton("No",
                    R.color.pdlg_color_red,
                    R.color.pdlg_color_white,
                    object : PrettyDialogCallback {
                        override fun onClick() {
                            pDialog.dismiss()
                        }
                    })
                .show()
    }
    private fun showCustomDialog() {
        val pDialog = PrettyDialog(this)
        pDialog
            .setTitle("Message")
            .setMessage("Are you sure you want to Logout?")
            .setIcon(R.drawable.pdlg_icon_info)
            .setIconTint(R.color.color_primary)
            .addButton(
                "Yes",
                R.color.pdlg_color_white,
                R.color.color_primary
            ) {
                Utilities.clearSharedPref(this)
                startActivity(Intent(this, LogInActivity::class.java))
                finish()
                pDialog.dismiss()
            }
            .addButton(
                "No",
                R.color.pdlg_color_white,
                R.color.pdlg_color_red
            ) { pDialog.dismiss() }
            .show()
    }

    override fun onBackPressed() {

        if (drawer!!.isDrawerOpen(GravityCompat.START)) {
            drawer!!.closeDrawer(GravityCompat.START)
        }
            showCustomDialogforExit()

    }
//    fun onnn(){
//
//
//    }

}