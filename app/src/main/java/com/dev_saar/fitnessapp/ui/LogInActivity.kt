package com.dev_saar.fitnessapp.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.dev_saar.fitnessapp.Utilities
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivityLogInBinding
import com.dev_saar.fitnessapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import com.mtechsoft.compassapp.models.authModels.UserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LogInActivity : BaseActivityWithoutVM<ActivityLogInBinding>() {

    private lateinit var apiClient: ApiClient
    lateinit var loginStatus:String
    override fun getViewBinding(): ActivityLogInBinding =
        ActivityLogInBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        apiClient = ApiClient()


        mViewBinding.apply {
            tvSignup.setOnClickListener {

                val intent = Intent(this@LogInActivity, SignupActivity::class.java)
                startActivity(intent)
            }
              tvSignup1.setOnClickListener {

                val intent = Intent(this@LogInActivity, SignupActivity::class.java)
                startActivity(intent)
            }
            btnLogIn.setOnClickListener {

                val getEmail: String = etEmailId.text.toString()
                val getPass: String = etPasswordId.text.toString()
                if (!getEmail.equals("")) {
                    if (!getEmail.equals("")) {
                        loginApi(getEmail, getPass)

                    } else {
                        showToast("Enter Password")
                    }
                } else {
                    showToast("Enter Email Address")

                }
            }
        }
    }

    fun loginApi(phone: String, pass: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().login(phone, pass)
            .enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == 200) {
                        hud.dismiss()
                        showToast(loginResponse.message)
                        val intent = Intent(this@LogInActivity, HomeActivity::class.java)
                        Utilities.saveString(
                            this@LogInActivity,
                            Utilities.USER_ID,
                            loginResponse.user.id
                        )
                        Utilities.saveString(this@LogInActivity, "loginStatus", "yes")
                        Utilities.saveString(this@LogInActivity, Utilities.USER_FNAME, loginResponse.user.first_name)
                        Utilities.saveString(this@LogInActivity, Utilities.USER_lNAME, loginResponse.user.last_name)
                        Utilities.saveString(this@LogInActivity, Utilities.USER_EMAIL, loginResponse.user.email)
                        Utilities.saveString(this@LogInActivity, "loginStatus", "yes")
                        startActivity(intent)
                        finish()


                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }
}