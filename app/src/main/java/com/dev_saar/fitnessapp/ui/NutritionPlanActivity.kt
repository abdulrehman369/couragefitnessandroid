package com.dev_saar.fitnessapp.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dev_saar.fitnessapp.R
import com.dev_saar.fitnessapp.Utilities
import com.dev_saar.fitnessapp.adapter.NutritionPlanAdapter
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivityNutritionPlanBinding
import com.dev_saar.fitnessapp.externalClasses.DownloadFile
import com.dev_saar.fitnessapp.model.dwnloadPDF.DownloadPdfDataModel
import com.dev_saar.fitnessapp.model.dwnloadPDF.MainDownloadPDFResponse
import com.dev_saar.fitnessapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NutritionPlanActivity : BaseActivityWithoutVM<ActivityNutritionPlanBinding>() {
    private lateinit var apiClient: ApiClient
    var userId: String = ""
    var type: String = ""
    lateinit var recyclerview: RecyclerView
    lateinit var tvStatus: RecyclerView
    var models = ArrayList<DownloadPdfDataModel>()
    lateinit var hud: KProgressHUD
    var URL = "http://www.codeplayon.com/samples/resume.pdf"
    override fun getViewBinding(): ActivityNutritionPlanBinding =
            ActivityNutritionPlanBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        type=Utilities.getString(this,"type")

        recyclerview = findViewById(R.id.rvNutritionPlane)
        tvStatus = findViewById(R.id.rvNutritionPlane)
        apiClient = ApiClient()
        userId = Utilities.getString(this, Utilities.USER_ID)
        callApi()
        mViewBinding.apply {
            if (type.equals("nutrition")){
tvTitle.text="Nutrition Plan"
            }else{
                tvTitle.text="Training Plan"

            }
            icBackArrow.setOnClickListener {

                onBackPressed()
            }



        }
    }

    fun callApi() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show()

        apiClient.getApiService().downloadPdf(type)
                .enqueue(object : Callback<MainDownloadPDFResponse> {
                    override fun onFailure(call: Call<MainDownloadPDFResponse>, t: Throwable) {
                        hud.dismiss()
                        Toast.makeText(
                                applicationContext,
                                "failure case:   " + t.message,
                                Toast.LENGTH_SHORT
                        )
                                .show()
//                        tvStatus.visibility= View.VISIBLE
//
                    }

                    override fun onResponse(
                            call: Call<MainDownloadPDFResponse>,
                            response: Response<MainDownloadPDFResponse>
                    ) {
                        val loginResponse = response.body()

                        models = response.body()!!.suggestedList
                        if (loginResponse?.statusCode == 200) {
                            hud.dismiss()
                            recyclerview.apply {
                                setHasFixedSize(true)
                                layoutManager =
                                        LinearLayoutManager(this@NutritionPlanActivity, LinearLayoutManager.VERTICAL, false)
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                                adapter = NutritionPlanAdapter(this@NutritionPlanActivity, models)
                            }
//                            if (models.isNullOrEmpty()){
//                                tvStatus.visibility= View.VISIBLE
//
//                            }else{
//                                tvStatus.visibility= View.GONE
//
//                            }

                        } else {
//                        val loginResponse = response.body()
                            hud.dismiss()
                            if (loginResponse != null) {
                                Toast.makeText(
                                        applicationContext,
                                        "" + loginResponse.message,
                                        Toast.LENGTH_SHORT
                                ).show()
                            }
//                            tvStatus.visibility= View.VISIBLE

                        }
                    }
                })
    }
    fun downloadPDF() {
        DownloadFile().execute(URL, URL)
    }
}
