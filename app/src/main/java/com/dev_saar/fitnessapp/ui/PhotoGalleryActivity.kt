package com.dev_saar.fitnessapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dev_saar.fitnessapp.R
import com.dev_saar.fitnessapp.Utilities
import com.dev_saar.fitnessapp.adapter.GetPhotoAdapter
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivityPhotoGalleryBinding
import com.dev_saar.fitnessapp.model.addImagesModels.MainGetImageResponse
import com.dev_saar.fitnessapp.model.addImagesModels.UserImageMainModel
import com.dev_saar.fitnessapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PhotoGalleryActivity : BaseActivityWithoutVM<ActivityPhotoGalleryBinding>() {

    private lateinit var apiClient: ApiClient
    var userId: String = ""
    lateinit var rvPhotoGallery: RecyclerView
    var sugestedmodel = ArrayList<UserImageMainModel>()
    lateinit var hud: KProgressHUD
    var getMonthNumber:Int=0
    var  spinnerName:String=""
    var isImageFitToScreen = false
    lateinit var tvStatus:TextView

    override fun getViewBinding(): ActivityPhotoGalleryBinding =
            ActivityPhotoGalleryBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        rvPhotoGallery = findViewById(R.id.rv_photo_gallery)
        tvStatus = findViewById(R.id.tvStatus)
        apiClient = ApiClient()
        userId = Utilities.getString(this, Utilities.USER_ID)
        init()

        mViewBinding.apply {

            icBackArrow.setOnClickListener {

                onBackPressed()
            }
            ivdBuilder.setOnClickListener{
                if (isImageFitToScreen) {
                    isImageFitToScreen = false
                    ivdBuilder.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    )
                    ivdBuilder.setAdjustViewBounds(true)
                } else {
                    isImageFitToScreen = true
                    ivdBuilder.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        )
                    )
                    ivdBuilder.setScaleType(ImageView.ScaleType.FIT_XY)
                }
            }
            icAdd.setOnClickListener {
                val intent = Intent(this@PhotoGalleryActivity, AddImageActivity::class.java)
                startActivity(intent)
            }

            val items = listOf(
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            )
//            val arrayAdapter = ArrayAdapter(this@PhotoGalleryActivity, R.layout.dropdown_item, items)
//            mViewBinding.tvMontNameAutoComplete.setAdapter(arrayAdapter)
            val spinner = findViewById<Spinner>(R.id.spinner)
            if (spinner != null) {
//                val adapter = ArrayAdapter(this@PhotoGalleryActivity,
//                        android.R.layout.simple_spinner_item, items)
                val yourAdapter: ArrayAdapter<String>

                yourAdapter = ArrayAdapter<String>(
                    this@PhotoGalleryActivity,
                    R.layout.spinner_text,
                    items
                )
                spinner.setAdapter(yourAdapter)

//                spinner.adapter = adapter

                spinner.onItemSelectedListener = object :
                        AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View, position: Int, id: Long
                    ) {
                        spinnerName = parent.getItemAtPosition(position).toString()
                        getMonthNumber=position+1
                        callApi(getMonthNumber.toString(),spinnerName)

                        showToast(spinnerName.toString())
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                        // write code to perform some action
                    }
                }
            }

        }
    }

    private fun init() {
//        callApi()

    }


    fun callApi(getMonthsNumber: String,spinnerName:String) {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show()

        apiClient.getApiService().getImages(userId, getMonthsNumber)
                .enqueue(object : Callback<MainGetImageResponse> {
                    override fun onFailure(call: Call<MainGetImageResponse>, t: Throwable) {
                        hud.dismiss()
                        Toast.makeText(
                            applicationContext,
                            "failure case:   " + t.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        tvStatus.visibility = View.VISIBLE
                        tvStatus.text="No Gallery Found According "+spinnerName
                    }

                    override fun onResponse(
                        call: Call<MainGetImageResponse>,
                        response: Response<MainGetImageResponse>
                    ) {
                        val loginResponse = response.body()

                        sugestedmodel = response.body()!!.user.suggestedList
                        if (loginResponse?.statusCode == 200) {
                            hud.dismiss()
                            rvPhotoGallery.apply {
                                setHasFixedSize(true)
                                layoutManager =
                                    LinearLayoutManager(
                                        this@PhotoGalleryActivity,
                                        LinearLayoutManager.VERTICAL,
                                        false
                                    )
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                                adapter = GetPhotoAdapter(this@PhotoGalleryActivity, sugestedmodel)
                            }
                            if (sugestedmodel.isNullOrEmpty()) {
                                tvStatus.visibility = View.VISIBLE
                                tvStatus.text="No Gallery Found According "+spinnerName

                            } else {
                                tvStatus.visibility = View.GONE

                            }

                        } else {
//                        val loginResponse = response.body()
                            hud.dismiss()
                            if (loginResponse != null) {
                                Toast.makeText(
                                    applicationContext,
                                    "" + loginResponse.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            tvStatus.visibility = View.VISIBLE
                            tvStatus.text="No Gallery Found According\n "+spinnerName

                        }
                    }
                })
    }

}