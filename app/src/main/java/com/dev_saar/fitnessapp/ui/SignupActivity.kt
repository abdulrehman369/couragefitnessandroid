package com.dev_saar.fitnessapp.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivitySignupBinding
import com.dev_saar.fitnessapp.model.CommonResponse
import com.dev_saar.fitnessapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupActivity : BaseActivityWithoutVM<ActivitySignupBinding>() {

    private lateinit var apiClient: ApiClient
    override fun getViewBinding(): ActivitySignupBinding =
        ActivitySignupBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        apiClient = ApiClient()


        mViewBinding.apply {
            tvLogin.setOnClickListener {
                val intent = Intent(this@SignupActivity, LogInActivity::class.java)
                startActivity(intent)
            }
            btnLogIn.setOnClickListener {

                val getFName: String = etFirstName.text.toString()
                val getLastName: String = etSecondName.text.toString()
                val getEmail: String = etEmailId.text.toString()
                val getPass: String = etPasswordId.text.toString()
                if (!getFName.equals("")) {
                    if (!getLastName.equals("")) {
                        if (!getEmail.equals("")) {
                            if (android.util.Patterns.EMAIL_ADDRESS.matcher(getEmail).matches()) {
                                if (!getEmail.equals("")) {
                                    callApi(getFName, getLastName, getEmail, getPass)
                                } else {
                                    showToast("Enter Password")
                                }
                            } else {
                                showToast("Enter Valid Email Address")

                            }
                        } else {
                            showToast("Enter Email Address")

                        }
                    } else {
                        showToast("Enter Last Name")

                    }
                } else {
                    showToast("Enter First Address")

                }
            }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_log_in)
        }
    }

    fun callApi(fName: String, lName: String, email: String, pass: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().signUp(fName, lName, email, pass)
            .enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<CommonResponse>,
                    response: Response<CommonResponse>
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == 200) {
                        hud.dismiss()
                        showToast(loginResponse.message)

                        val intent = Intent(this@SignupActivity, LogInActivity::class.java)
                        startActivity(intent)


                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }
}