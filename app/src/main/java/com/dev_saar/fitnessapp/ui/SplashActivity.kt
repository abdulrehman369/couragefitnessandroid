package com.dev_saar.fitnessapp.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.dev_saar.fitnessapp.R
import com.dev_saar.fitnessapp.Utilities


class SplashActivity : AppCompatActivity() {
    private val progressBar: ProgressBar? = null
    private val pStatus = 0
    private val handler = Handler()
    private val SPLASH_TIME_OUT = 2000L
    lateinit var login_status: String
     var isLogin:String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        isLogin= Utilities.getString(this,"loginStatus")
        if (isLogin.equals("yes")) {

            Handler().postDelayed(
                {
                    val i = Intent(this@SplashActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()
                }, SPLASH_TIME_OUT
            )

        } else {
            Handler().postDelayed(
                {
                    val i = Intent(this@SplashActivity, LogInActivity::class.java)
                    startActivity(i)
                    finish()
                }, SPLASH_TIME_OUT
            )
        }
        val sharedPreference =
            getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)



    }


}
