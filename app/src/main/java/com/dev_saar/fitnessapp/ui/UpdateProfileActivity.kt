package com.dev_saar.fitnessapp.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.dev_saar.fitnessapp.Utilities
import com.dev_saar.fitnessapp.base.BaseActivityWithoutVM
import com.dev_saar.fitnessapp.databinding.ActivityUpdateProfileBinding
import com.dev_saar.fitnessapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import com.mtechsoft.compassapp.models.authModels.UserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateProfileActivity : BaseActivityWithoutVM<ActivityUpdateProfileBinding>() {
    var userId: String = ""
    private lateinit var apiClient: ApiClient
    override fun getViewBinding(): ActivityUpdateProfileBinding =
        ActivityUpdateProfileBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        apiClient = ApiClient()
        userId = Utilities.getString(this, Utilities.USER_ID)
       val fName = Utilities.getString(this, Utilities.USER_FNAME)
        val lName = Utilities.getString(this, Utilities.USER_lNAME)
        val email= Utilities.getString(this, Utilities.USER_EMAIL)

        mViewBinding.apply {
            etFirstName.setText(fName)
            etSecondName.setText(lName)
            etEmailId.setText(email)
            icBackArrow.setOnClickListener {

                onBackPressed()
            }

            llChangePass.setOnClickListener {
                val intent = Intent(this@UpdateProfileActivity, ChangePassActivity::class.java)
startActivity(intent)
            }
            btnProfile.setOnClickListener {

                val getFName: String = etFirstName.text.toString()
                val getLastName: String = etSecondName.text.toString()
                val getEmail: String = etEmailId.text.toString()
                if (!getFName.equals("")) {
                    if (!getLastName.equals("")) {
                        if (!getEmail.equals("")) {
                            if (android.util.Patterns.EMAIL_ADDRESS.matcher(getEmail).matches()) {
                                if (!getEmail.equals("")) {
                                    callApi(userId,getFName, getLastName, getEmail)
                                } else {
                                    showToast("Enter Password")
                                }
                            } else {
                                showToast("Enter Valid Email Address")

                            }
                        } else {
                            showToast("Enter Email Address")

                        }
                    } else {
                        showToast("Enter Last Name")

                    }
                } else {
                    showToast("Enter First Name")

                }
            }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_log_in)
        }
    }

    fun callApi(userId:String,fName: String, lName: String, email: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().updateProfileApi(userId,fName, lName, email)
            .enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == 200) {
                        hud.dismiss()
                        showToast(loginResponse.message)

                        val intent = Intent(this@UpdateProfileActivity, HomeActivity::class.java)
                        Utilities.saveString(this@UpdateProfileActivity, Utilities.USER_FNAME, loginResponse.user.first_name)
                        Utilities.saveString(this@UpdateProfileActivity, Utilities.USER_lNAME, loginResponse.user.last_name)
                        Utilities.saveString(this@UpdateProfileActivity, Utilities.USER_EMAIL, loginResponse.user.email)
                        startActivity(intent)
finish()

                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }
}